<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="OpenApi Documentation",
 *      description="Документация для микро сервиса",
 * )
 *
 * @OA\Server(
 *      url=L5_SWAGGER_CONST_HOST,
 *      description="Основной API"
 * )
 */

class AppBaseController extends Controller
{
    use ApiResponser;

}
