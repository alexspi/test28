<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\MarkResource;
use App\Models\Mark;
use Illuminate\Http\Request;

class MarkApiController extends AppBaseController
{
    public function getMark()
    {

//        dd(Mark::with('models')->get());
        return $this->successResponse(MarkResource::collection(Mark::with('models')->get()), 'successfully');
    }
}
