<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\MarkResource;
use App\Http\Resources\UserCarResource;
use App\Models\Car;
use App\Models\Mark;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CarApiController extends AppBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): \Illuminate\Http\Response
    {

        $cars = Car::where('user_id', Auth::user()->id)->with(['mark', 'carModel'])->get();
        if (!$cars) {
            return $this->errorResponse('У вса нет машин');
        }

        return $this->successResponse(UserCarResource::collection($cars), 'succes');
    }

    /**
     * Show the form for creating a new resource.
     * тут я подразумеваю, что на фронт если он есть передается
     * список марок с моделями, и там (на фронте) они будут зависимыми селектами
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->tokenCan('car:crud')) {
            return $this->errorResponse('У вса нет права создавать');
        }

        return $this->successResponse(MarkResource::collection(Mark::with('models')->get()));
    }

    /**
     * @OA\Post(
     *      path="/api/car/create",
     *      operationId="store",
     *      summary="Создание машины пользователем",
     *      description="Метод возвращает созданный экземпляр  ...",
     *     @OA\Parameter(
     *          name="mark_id",
     *          description="id марки машины",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="model_id",
     *          description="id модели мошины",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="options.years",
     *          description="год выпуска машины",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *                     )
     *      ),
     *     @OA\Parameter(
     *          name="options.color",
     *          description="цвет машины",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *                     )
     *      ),
     *    @OA\Parameter(
     *          name="options.mileage",
     *          description="пробег машины",
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *                     )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *     @OA\Response(
     *          response=400,
     *          description="Упс вы ввели неправильные данные",
     *     ),
     *      @OA\Response(
     *          response=403,
     *          description="У вса нет права создавать",
     *     )
     *
     * )
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mark_id' => 'required|numeric',
            'model_id' => 'required|numeric',
            'options.years' => 'date_format:Y',
            'options.color' => 'alpha_dash',
            'options.mileage' => 'numeric',
        ]);

        if ($validator->fails()) {
            return $this->errorResponse('Упс вы ввели неправильные данные', '400');
        }

        if (!Auth::user()->tokenCan('car:crud')) {
            return $this->errorResponse('У вса нет права создавать','403');
        }

        $car = Car::create([
            'mark_id' => $request->mark_id,
            'model_id' => $request->model_id,
            'user_id' => Auth::user()->id,
        ]);

        if ($request->options) {
            $car->update([
                'options' => $request->options
            ]);
        }

        return $this->successResponse(new UserCarResource($car), 'машина успешно добавлена');


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $car = Car::where('user_id', Auth::user()->id)->whereId($id)->first();

        if (!$car) {
            return $this->errorResponse('У вса нет Нет такой машины', 500);
        }

        return $this->successResponse(new UserCarResource($car));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->tokenCan('car:crud')) {

            return $this->errorResponse('У вса нет права редактировать');
        }
        $car = Car::where('user_id', Auth::user()->id)->whereId($id)->first();
        $car->update([
            'mark_id' => $request->mark_id,
            'model_id' => $request->model_id,
        ]);

        if ($request->options) {
            $car->update([
                'options' => $request->options
            ]);
        }

        return $this->successResponse(new UserCarResource($car), 'машина успешно обновлена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->tokenCan('car:crud')) {
            return $this->errorResponse('У вса нет права удалять', 500);
        }

        $car = Car::where('user_id', Auth::user()->id)->whereId($id)->first();

        if (!$car) {
            return $this->errorResponse('У вса нет Нет такой машины', 500);
        }

        $car->delete();
        return $this->successResponse(null, 'машина успешно удалена');
    }
}
