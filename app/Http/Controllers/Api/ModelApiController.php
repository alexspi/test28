<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\AllModelResource;
use App\Models\CarModel;
use Illuminate\Http\Request;

class ModelApiController extends AppBaseController
{
    /**
     * @return mixed
     */
    public function getAllModel(Request $request)
    {
        if ($request->id){
            $model = CarModel::where('mark_id',$request->id)->get();
            if (!$model) {
                return $model->errorResponse('Такой машины нет', '404');
            }
            return $this->successResponse(AllModelResource::collection($model), 'successfully');
        }


        return $this->successResponse(AllModelResource::collection(CarModel::all()), 'successfully');
    }


}
