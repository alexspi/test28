<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Car extends Model
{
    use HasFactory,SoftDeletes;


    protected $fillable = [
        'mark_id',
        'model_id',
        'user_id',
        'options'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'array',
    ];

    public function mark()
    {
        return $this->belongsTo(Mark::class, 'mark_id', 'id');
    }



    public function carModel()
    {
        return $this->belongsTo(CarModel::class, 'model_id', 'id');
    }


    public function user()
    {
        return $this->belongsTo(user::class, 'user_id', 'id');
    }
}
