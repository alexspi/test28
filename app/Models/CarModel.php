<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarModel extends Model
{
    use HasFactory;

    protected $table='car_models';

    public function mark(){
        return $this->belongsTo(Mark::class);
    }


    public function cars(){
        return $this->hasMany(Car::class,'id','model_id');
    }
}
