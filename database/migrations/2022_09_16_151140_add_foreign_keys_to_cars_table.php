<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->foreign(['mark_id'], 'FK_cars_marks')->references(['id'])->on('marks')->onUpdate('CASCADE')->onDelete('NO ACTION');
            $table->foreign(['user_id'], 'FK_cars_users')->references(['id'])->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['model_id'], 'FK_cars_models')->references(['id'])->on('car_models')->onUpdate('CASCADE')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->dropForeign('FK_cars_marks');
            $table->dropForeign('FK_cars_users');
            $table->dropForeign('FK_cars_models');
        });
    }
}
