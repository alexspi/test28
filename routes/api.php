<?php

use App\Http\Controllers\Api\CarApiController;
use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\MarkApiController;
use App\Http\Controllers\Api\ModelApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', [LoginController::class, 'login']);
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('getmark',[MarkApiController::class,'getMark']);
Route::get('getmodel',[ModelApiController::class,'getAllModel']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::get('car', [CarApiController::class, 'index']);
    Route::get('car/create', [CarApiController::class, 'create']);
    Route::post('car/create', [CarApiController::class, 'store']);
    Route::get('car/edit/{id}', [CarApiController::class, 'edit']);
    Route::post('car/edit/{id}', [CarApiController::class, 'update']);
    Route::delete('car/delete/{id}', [CarApiController::class, 'destroy']);
});